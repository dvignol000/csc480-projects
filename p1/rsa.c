#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf) 
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

typedef int bool;
#define TRUE  1
#define FALSE 0

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	/* NOTE: len may overestimate the number of bytes actually required. */
	unsigned char* buf = malloc(len);
	Z2BYTES(buf,len,x);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */
	unsigned char buffer[keyBits];

	NEWZ(cand);
	int isprime;
	bool qset = FALSE;
	bool pset = FALSE;

	size_t prime_length = keyBits / 2;

	// generate p and q primes
	do {
		// update buffer
		randBytes(buffer, prime_length);
		// convert buffer to integer
		BYTES2Z(cand,buffer, prime_length);

		// check if prime 
		// 2 =  definitely prime, 1 = probably prime, 0 = definitely non-prime.
		isprime = ISPRIME(cand);

		if(isprime != 0 && pset == FALSE){
			mpz_set(K->p,cand);
			pset = TRUE;
		}
		else if(isprime != 0 && qset == FALSE){
			mpz_set(K->q,cand);
			qset = TRUE;
		}
	}
	while(!(qset && pset));

	// set n = pq
	mpz_mul(cand, K->p, K->q);
	mpz_set(K->n, cand);

	// set phi(n)
	NEWZ(p_minus1);
	mpz_sub_ui(p_minus1, K->p, 1);

	NEWZ(q_minus1);
	mpz_sub_ui(q_minus1, K->q, 1);

	NEWZ(phi_n);
	mpz_mul(phi_n, q_minus1, p_minus1);

	// set e, 1<e<phi(n), e & phi(n) must be coprime
	mpz_set_ui(K->e, 65537);

	// set d = (e mod phi(n)) ^ -1
	mpz_invert(cand, K->e, phi_n);
	mpz_set(K->d, cand);
	
	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */

	//Output length written to outBuf
	size_t out_len = 0;
	
	//Long integer representation of m and c
	NEWZ(m);
	NEWZ(c);

	//Take the message from inBuf and convert to long int
	BYTES2Z(m, inBuf, len);

	//Perform exponentiation with e, mod n to retrieve ciphertext c
	mpz_powm(c, m, K->e, K->n);

	//Use c to write ciphertext to output buffer
	Z2BYTES(outBuf, out_len, c);
	return out_len; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  See remarks above. */
	//Output length written to outBuf
	size_t out_len = 0;
	
	//Long integer representation of m and c
	NEWZ(m);
	NEWZ(c);

	//Take the ciphertext from inBuf and convert to long int
	BYTES2Z(c, inBuf, len);
	
	//Perform exponentiation with d, mod n to retrieve message m
	mpz_powm(m, c, K->d, K->n);
	
	//Use m to write message back to output buffer
	Z2BYTES(outBuf, out_len, m);
	return out_len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
