#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

/* For the size of the file. */
#include <sys/stat.h>

#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext len will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */


	if(entropy == NULL)
		{
			//allocating memory for hmac and aes keys
	unsigned char* buff_hmac= malloc(HM_LEN);
	unsigned char* buff_aes= malloc(HM_LEN);

			//getting random hmac and aes keys using prf
		  randBytes(buff_hmac, HM_LEN);
			randBytes(buff_aes, HM_LEN);

			//copying random keys to fields in K,freeing memory
			memcpy(K->hmacKey,buff_hmac,HM_LEN);
			memcpy(K->aesKey,buff_aes,HM_LEN);
			free(buff_hmac);
			free(buff_aes);
		}
	else
		{
		unsigned char* keygen = malloc(64);
		//use KDF and entropy to get the keys, HMAC-SHA512 with KDF_KEY will be fine
			HMAC(EVP_sha512(),&KDF_KEY,32,entropy,entLen,keygen,NULL);
			//copying to fields of K and freeing memory
			memcpy(K->hmacKey,keygen,HM_LEN);
			memcpy(K->aesKey,keygen+32,HM_LEN);
			free(keygen);

		}
	return 0;
}

size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}


size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */

	//if IV is null, allocate 16 bytes for it and give it random bytes
		if (IV == NULL)
			{
				IV = malloc(16);
				randBytes(IV,16);
			}

	//initializing the cipher context(ctx) data structure that is needed for encryption
		EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
		if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
			{
			ERR_print_errors_fp(stderr);
			}

		//encrypt(generate ciphertext: second part of final result)
		int nWritten;
		unsigned char* ciphertext = malloc(len);
		if (1!=EVP_EncryptUpdate(ctx,ciphertext,&nWritten,inBuf,len))
			{
				ERR_print_errors_fp(stderr);
			}



		//nWritten holds the len of ciphertext. Now have to apply hmac to IV|C
		unsigned char* iv_ciphertext = malloc(16+nWritten);
		memcpy(iv_ciphertext,IV,16); // copy IV to first bytes of iv_ciphertext
		memcpy(iv_ciphertext+16,ciphertext,nWritten);  //copy ciphertext to remaining bytes of iv_ciphertext

		unsigned char* hmac_output = malloc(32);
		HMAC(EVP_sha256(),K->hmacKey,32,iv_ciphertext,16+nWritten,hmac_output,NULL);

		// now outbuf has to be set to the desired format which is: 16 byte IV | ciphertext | hmac of IV and ciphertext
			unsigned char* prep_Outbuf = malloc(16 + HM_LEN+nWritten);

			memcpy(prep_Outbuf , iv_ciphertext , 16+nWritten);
			memcpy(prep_Outbuf+16+nWritten , hmac_output , 32);  // since hmac with sha256 was used,hmac_output is 32 bytes

			memcpy(outBuf,prep_Outbuf,16+HM_LEN+nWritten);  //write to output

			//Lastly, free memory
			free(ciphertext);
			free(iv_ciphertext);
			free(hmac_output);
			free(prep_Outbuf);
			//free up ctx data structure
			EVP_CIPHER_CTX_free(ctx);

			return (16 + HM_LEN+nWritten); /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}


size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	    
	struct stat s;
	const char * mappedin;
    int fdin = open (fnin, O_RDONLY);
	int fdout = open(fnout, O_RDWR | O_CREAT , (mode_t)0600);

	// size of file
    int status = fstat (fdin, & s);
    size_t sizein = s.st_size;

    /* Memory-map the file. */
    mappedin = mmap (0, sizein, PROT_READ, MAP_PRIVATE, fdin, 0);

	if (mappedin == MAP_FAILED)
    {
        close(fdin);
        perror("mmap error fdin");
        exit(EXIT_FAILURE);
    }

	// find cipher len and encrypt to buffer
	size_t fdinlen = strlen(mappedin); 
	size_t cipherlen = ske_getOutputLen(fdinlen);
	unsigned char* ciphertext = malloc(cipherlen);
	ssize_t encryptlen = ske_encrypt(ciphertext, (unsigned char*) mappedin, fdinlen, K, IV);

	// write to fdout
	lseek(fdout, offset_out, SEEK_SET);
	ssize_t wout = write(fdout, ciphertext, encryptlen);


	// cleanup
	munmap(mappedin, sizein);
	free(ciphertext);
	close(fdin);
	close(fdout);

	return 0;
}


size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */

	// inbuf contains ciphertext(16 byte IV | AES ciphertext|hmac of (IV|C),32 bytes)

	//must check hmac to verify integrity (IV|C)

	unsigned char* iv_ciphertext = malloc(len-32);
	memcpy(iv_ciphertext,inBuf,len -32);
	unsigned char* hmac_output = malloc(32);

	//compute hmac of IV|C in inBuf, to later compare if last segment of inBuf is the same as the hmac computed
	HMAC(EVP_sha256(),K->hmacKey,32,iv_ciphertext,len-32,hmac_output,NULL);

	if(0!=memcmp(hmac_output,inBuf+len-HM_LEN,32))
		{
		return -1;
		}

	//need IV so will extract that from inBuf
	unsigned char* IV = malloc(16);
	memcpy(IV,inBuf,16);

	//decryption below
	int nWritten;
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		{
		ERR_print_errors_fp(stderr);
		}
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,inBuf+16,len-48))
		{
			ERR_print_errors_fp(stderr);
		}

	//free memory
	free(iv_ciphertext);
	free(hmac_output);
	free(IV);
	EVP_CIPHER_CTX_free(ctx);
	return nWritten;
}


size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	int fdin = open(fnin, O_RDONLY);
	int fdout = open(fnout, O_CREAT | O_RDWR, (mode_t)0600);
	
	// buffer status info
	struct stat s;
	fstat(fdin, &s);

	// map file to memory
	unsigned char *mappedin;
	mappedin = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fdin, offset_in);
	
	if (mappedin == MAP_FAILED) {
		printf("mmap error fdin");
		return -1;
	}

	// allocate and decrypt to buffer
	size_t sizein = s.st_size - offset_in;
	size_t messagelen = sizein - 16 - HM_LEN;
	unsigned char* message = malloc(messagelen);
	ssize_t decryptlen = ske_decrypt(message, mappedin, sizein, K);

	// write to fdout
	ssize_t wout = write(fdout, message, decryptlen);

	// cleanup
	munmap(mappedin, s.st_size);
	free(message);
	close(fdin);
	close(fdout);

	return 0;
}